plugins {
    java
    id("org.springframework.boot") version "3.1.1"
    id("io.spring.dependency-management") version "1.1.0"
}

group = "kz.codesmith"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    implementation("org.keycloak:keycloak-spring-boot-starter:21.1.0")
    implementation("org.keycloak:keycloak-spring-security-adapter:21.1.0")
    implementation("org.keycloak:keycloak-admin-client:21.0.1")
    implementation ("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("org.projectlombok:lombok:1.18.28")
}

tasks.withType<Test> {
    useJUnitPlatform()
}
