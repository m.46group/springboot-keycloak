package kz.codesmith.keycloakjwt.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@Validated
public class MyServiceController {


    @GetMapping("/user")
    @PreAuthorize("hasAnyRole('user')")
    public String myAppService() {
        return "Hi user";
    }

    @PostMapping("/admin")
    @PreAuthorize("hasAnyRole('admin')")
    public String myadmin() {
        return "Hi admin";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasAnyRole('admin')")
    public String myadmin2() {
        return "Hi admin";
    }

    @GetMapping("/noauth")
    public String noauth() {
        return "Hi anonymous";
    }

}